using System;
using System.Collections.Generic;
using PartyInvites.Controllers;
using PartyInvites.Models;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using System.Linq;


namespace Tests
{
    public class HomeControllerTests
    {
        [Fact]
        public void ListActionFiltersNonAtendees(){
            HomeController controller = new HomeController(new FakeRepository());
            ViewResult result = controller.ListResponses();

            Assert.Equal(2, (result.Model as IEnumerable<GuestResponse>).Count());
        }
    }
}