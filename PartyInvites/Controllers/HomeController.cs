using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PartyInvites.Models;

namespace PartyInvites.Controllers
{
    public class HomeController: Controller
    {
        private readonly IRepository repo;

        public HomeController(IRepository repos)
        {
            this.repo = repos;
        }

        public ViewResult Index() {
            int hour = DateTime.Now.Hour;

            ViewBag.Greeting = hour < 12 ? "Good morning" : "Good afternoon";

            return View("MyView");
        }

        [HttpGet]
        public ViewResult RsvpForm() => View();

        [HttpPost]
        public ViewResult RsvpForm(GuestResponse guestResponse){
            if(ModelState.IsValid){
                repo.AddResponse(guestResponse);
                return View("Thanks", guestResponse);
            }
            else {
                return View();
            }
            
        }

        public ViewResult ListResponses() => 
            View(repo.Responses.Where(r => r.WillAttend == true));


    }
}